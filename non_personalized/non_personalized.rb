# NPRS - non-personalized recommendation system
require 'set'
require_relative 'Film'
require_relative 'Rating'

class NPRS

	def initialize(ratings, films)
		@ratings = ratings
		@films = films
	end

	def average ()
		puts "Avarage algorithm"

		@ratings.each do |rating|
			film = @films.detect{ |x| x.id == rating.film_id }
			film.count = film.count + 1
			film.sum = film.sum + rating.rating
		end

		@films.each do |film| 
			film.rating = ( film.sum.to_f / film.count ).round(2)
		end

		top_ten(@films)
	end

	def netvotes()
		puts "Netvotes algorithm"

		@ratings.each do |rating|
			film = @films.detect{ |x| x.id == rating.film_id }
			
			if rating.rating > 5
				film.up = film.up + 1
			else
				film.down = film.down + 1
			end
		end

		@films.each{ |film| film.rating = film.up - film.down }

		top_ten(@films)
	end

	def positive()
		puts "Positive algorithm"

		@ratings.each do |rating|
			film = @films.detect{ |x| x.id == rating.film_id }
			film.count = film.count + 1
			
			if rating.rating > 7
				film.positive = film.positive + 1 
			end
		end

		@films.each do |film| 
			film.rating = ( 100 * film.positive.to_f / film.count ).round(2)
		end

		top_ten(@films)
	end

	def association(film_id)
		puts "Association algorithm"
		users = []

		films_with_ratings = @ratings.group_by{ |x| x.film_id }
		targeted_film = films_with_ratings[film_id]

		abort("Film: #{film_id} wasn't found") if targeted_film.nil?

		x = targeted_film.map{ |rating| rating.rating > 7 ? 				# ids of users who like film
														rating.user_id : 0 }.to_set
		
		users = @ratings.group_by{ |x| x.user_id }.keys.to_set			# ids of all users
		_x = users - x 																							# ids of users who didn't see film

		films_with_ratings.each do |id, ratings|
			unless id == film_id
				y = xy =_xy = 0

				current_user_set = ratings.map{ |r| r.rating > 7 ?			# ids of users who like current film (y)
																				r.user_id : 0 }.to_set 
				
				xy = (x & current_user_set)															# ids of users who like both films
				_xy = (_x & current_user_set)														# ids of users who don't like targeted film but current film

				film = @films.detect{ |x| x.id == ratings[0].film_id }
				film.rating = ( (xy.length.to_f/x.length) / (_xy.length.to_f/_x.length) ).round(2)  unless film.nil?
			end
		end

		top_ten(@films)
	end

	def top_ten(films)
		top = films.sort_by{ |film| film.rating }.reverse
		top.take(10)
	end

	def print_result()
		top = @films.sort_by{ |film| film.rating }.reverse
		top = top.take(10)

		puts "\nResults:"

		top.each_with_index do |film, i|
			puts "#{i+1}. #{film.title} #{film.rating}"
		end
		
		puts "---"
	end

end