# data model
class Rating
	attr_reader :user_id, :film_id, :rating

	def initialize(user_id, film_id, rating)
		@user_id = user_id
		@film_id = film_id
		@rating = rating
	end

end