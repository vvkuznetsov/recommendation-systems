# data model
class Film
	attr_reader :id, :title
	attr_accessor :count, :sum, :rating, :up, :down, :positive

	def initialize(id, title)
		@id = id
		@title = title
		@count =  @sum =  @rating = 0
		@up =  @down =  @positive = 0
	end

end