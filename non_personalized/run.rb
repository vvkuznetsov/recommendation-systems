#!/usr/bin/ruby

require 'optparse'
require 'yaml'
require_relative 'non_personalized'
require_relative 'Film'
require_relative 'Rating'

args = {}
OptionParser.new do |opts|
  opts.banner = "Usage: example.rb [options]"

  opts.on('-r', '--ratings FILE_NAME', 'CSV file with ratings') { |v| args[:ratings] = v }
  opts.on('-t', '--titles FILE_NAME', 'CSV file with titles') { |v| args[:titles] = v }
  opts.on('-m', '--method METHOD_NAME', 'Name of algorithm to use') { |v| args[:method] = v }
  opts.on('-x', '--film FILM_NAME', 'Film name (association method only') { |v| args[:film_id] = v.to_i }
  opts.on('-o', '--output FILM_NAME', 'File name of the file for output') { |v| args[:output] = v }

end.parse!

options = YAML.load_file('default.yaml')
options.update(args)

puts "Reading data from files..."
print "... #{options[:ratings]}"

lines = File.readlines(options[:ratings])
lines.shift
ratings = Array.new(lines.length)

lines.each_with_index do |line, i|
	user_id, film_id, rating = line.chomp.split('|')
	ratings[i] = Rating.new(user_id.to_i, film_id.to_i, rating.to_i)
end

puts "\tok"

print "... #{options[:titles]}"
lines = File.readlines(options[:titles])
lines.shift
films = Array.new(lines.length)

lines.each_with_index do |line, i|
	id, title = line.chomp.split('|')
	films[i] = Film.new(id.to_i, title)
end

puts "\tok\n\n"

nprs = NPRS.new(ratings, films)

top_ten = case options[:method]
when "average" then nprs.average
when "netvotes" then nprs.netvotes
when "positive" then nprs.positive
when "association" then nprs.association(options[:film_id])
else abort("Algorithm name didn't recognized. Use: average, netvotes, positive, association")
end


print "\nPrinting results to #{options[:output]}..."

File.open(options[:output], 'w') do |f|
  f.puts "film_id|score"

  top_ten.each do |film|
    f.puts "#{film.id}|#{film.rating}"
  end
end

puts "\tok"
puts "---"